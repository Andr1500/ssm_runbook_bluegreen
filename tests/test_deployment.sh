#!/bin/bash

ALB_ENDPOINT="your_alb_endpoint"

COOKIE_JAR="cookies.txt"

while true; do
    RESPONSE=$(curl -b $COOKIE_JAR -c $COOKIE_JAR -sI $ALB_ENDPOINT)
    HTTP_STATUS=$(echo "$RESPONSE" | grep HTTP | awk '{print $2}')

    if [ "$HTTP_STATUS" -eq 200 ]; then
        # Success: Display the content within <h1> tags
        CONTENT=$(curl -b $COOKIE_JAR -s $ALB_ENDPOINT | grep '<h1>' | sed 's/<[^>]*>//g')
        echo "HTTP Status Code: $HTTP_STATUS"  >> deployment_status.txt
        echo "Response: $CONTENT"  >> deployment_status.txt
    else
        # Error: Display the HTTP status code
        echo "HTTP Status Code: $HTTP_STATUS"   >> deployment_status.txt
    fi

    sleep 1
done