# Blue/Green deployment on AWS EC2 instances with Systems Manager Automation runbook

My posts on Medium about the project: 
1. [Blue/Green deployment on AWS EC2 instances with SM Automation runbook](https://medium.com/p/ce1643ef642c)
2. [CodePipeline pipeline for automation Blue/Green deployment with SM Automation runbook](https://medium.com/p/90b3c65885db)
3. [Monitoring EC2 instances deployed with Blue/Green deployment](https://medium.com/p/4ae36c6cecdf)


## Pre-reqs

Before you start, make sure the following requirements are met:
- An AWS account with permissions to create resources.
- AWS CLI installed on your local machine.

## Building & Running 

1. Clone the repository, go into it, and create an S3 bucket for nested stack templates and application files

on Linux:
```date=$(date +%Y%m%d%H%M%S)```

on Windows PowerShell:
```$date = Get-Date -Format "yyyyMMddHHmmss"```

```
aws s3api create-bucket --bucket cloudformation-app-files-${date} --region YOUR_REGION \
  --create-bucket-configuration LocationConstraint=YOUR_REGION
```

2. Add policy to the S3 bucket for access from the deployed EC2 instance

```
aws s3api put-bucket-policy --bucket cloudformation-app-files-${date} --policy '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"Service":"ec2.amazonaws.com"},"Action":"s3:GetObject","Resource":"arn:aws:s3:::'"cloudformation-app-files-${date}"'/*"}]}'
```

3. Fill in all necessary Parameters in the root.yaml file  and send all nested stack files to the S3 bucket

```
aws s3 cp infrastructure s3://cloudformation-app-files-${date}/infrastructure  --recursive
```

4. Go to the  `infrastructure` directory and create CloudFormation stack

```
aws cloudformation create-stack \
    --stack-name ec2-bluegreen-deployment \
    --template-body file://root.yaml \
    --capabilities CAPABILITY_NAMED_IAM \
    --disable-rollback
```

5. Open your mailbox and confirm your subscription to the SNS topic.

Access to the EC2 instance is possible through the Systems Manager. Go to AWS console -> AWS Systems Manager -> Fleet Manager -> choose the created EC2 instance -> Node actions -> Connect -> Start terminal session. Here you can check if everything was created and configured correctly.

6. For manual deployment. 
Send all application files to the S3 bucket. Start Systems Manager Automation Runbook execution, if everything is ok - you receive an email with the information "Deployment Status: Success" and will have WEB access to the deployed EC2 instance through the WEB browser, in case of any failure - you receive an email with information "Deployment Status: Failed" and details about the failed step. For deployment of a new version of the application - make changes in application/index.html, send changes to the S3 bucket, and start the Systems Manager Automation Runbook again.

```
aws s3 cp application s3://cloudformation-app-files-${date}/application  --recursive
aws ssm start-automation-execution --document-name "Ec2BlueGreenDeployment"
```

For automated deployment with CodePipeline pipeline.
Fill all necessary Parameters in the `infrastructure/codepipeline_pipeline.yaml` file, go to the `infrastructure` directory, create CloudFormation stack, after succesfull creation update CodeStar pending connection, deploy new version of the application to your remote repository.

```
aws cloudformation create-stack \
    --stack-name codepipeline-pipeline\
    --template-body file://codepipeline_pipeline.yaml \
    --capabilities CAPABILITY_NAMED_IAM --disable-rollback
```


For automated deployment with CodePipeline pipeline.
Fill all necessary Parameters in the `infrastructure/codepipeline_pipeline.yaml` file, go to the `infrastructure` directory, create CloudFormation stack, after succesfull creation update CodeStar pending connection, deploy new version of the application to your remote repository.

```
aws cloudformation create-stack \
    --stack-name codepipeline-pipeline\
    --template-body file://codepipeline_pipeline.yaml \
    --capabilities CAPABILITY_NAMED_IAM --disable-rollback
```

7. Momitoring the EC2 instance deployed with blue/green deployment.

Fill all necessary Parameters in the `infrastructure/ec2_monitoring.yaml`, go to the `infrastructure` directory, create CloudFormation stack.

```
aws cloudformation create-stack \
    --stack-name ws-ec2-monitoring \
    --template-body file://ec2_monitoring.yaml \
    --capabilities CAPABILITY_NAMED_IAM --disable-rollback
```

8. Deletion of the CloudFormation stacks. Terminate unnecessary EC2 instance and delete the CloudFormation stacks.

```
aws cloudformation delete-stack --stack-name ws-ec2-monitoring

aws cloudformation delete-stack --stack-name codepipeline-pipeline

aws cloudformation delete-stack --stack-name ec2-bluegreen-deployment
```

9. Deletion all files from the S3 bucket and deletion of the S3 bucket

```
aws s3 rm s3://cloudformation-app-files-${date} --recursive
```

```
aws s3 rb s3://cloudformation-app-files-${date} --force
``` 

## Deployment and infrastructure schemas


*Infrastructure schema*

![infrastructure_schema](images/ssm_bluegreen_full_schema.png)


*Runbook steps schema*

![runbook_schema](images/runbook_schema.png)


*CodePipeline pipeline schema*

![pipeline](images/codepipeline.png)


*CloudWatch EC2 metrics*

![cw_metrics](images/cw_ec2_metrics.png)

You can support me with a virtual coffee https://www.buymeacoffee.com/andrworld1500 .